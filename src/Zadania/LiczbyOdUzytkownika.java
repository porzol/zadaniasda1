package Zadania;

import java.util.Scanner;

public class LiczbyOdUzytkownika {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe liczb: ");
        int ilosc = sc.nextInt();
        int[] tablica = new int[ilosc];
        for (int i = 0; i < ilosc; i++) {
            System.out.println("Podaj kolejne liczby");
            tablica[i] = sc.nextInt();
        }
        int min = 0;
        for (int i = 0; i < tablica.length; i++) {
            if (i == 0) {
                min = tablica[0];
            } else if (tablica[i] < tablica[i - 1]) {
                min = tablica[i];
            }
        }
        System.out.println(min);
    }
}
