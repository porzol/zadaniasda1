package Zadania;

import java.util.Random;

public class Zadanie24 {
    public static void main(String[] args) {

        Random los = new Random();
        int[] tab = new int[10];

        for (int i = 0; i < tab.length; i++) {
            tab[i] = los.nextInt(21) - 10;
            System.out.print(tab[i] + " ");
        }
        double suma = 0;
        for (int i = 0; i < 10; i++) {
            suma += tab[i];
        }
        double srednia = suma / tab.length;
        System.out.println("\nSrednia to: " + srednia);

        System.out.println("Min: " + min(tab) + " Max: " + max(tab));
        int lWieksza = 0;
        for (int i = 0; i < 10; i ++){
            if (tab[i] > srednia){
                lWieksza++;
            }
        }
        System.out.println("Wiekszych od sredniej: " + lWieksza);
        int lMniejsza = 0;
        for (int i = 0; i < 10; i ++){
            if (tab[i] < srednia){
                lMniejsza++;
            }
        }
        System.out.println("Mniejszych od sredniej: " + lMniejsza);

        for (int i = 0; i < 10; i++) {
            System.out.print(tab[9- i] + " ");
        }

    }
    public static int min(int[] tabela) {
        int min = 0;
        for (int i = 0; i < tabela.length; i++) {
            if (i == 0) {
                min = tabela[0];
            } else if (tabela[i] <= min) {
                min = tabela[i];
            }
        }
        return min;
    }

    public static int max(int[] tabela) {
        int max = 0;
        for (int i = 0; i < tabela.length; i++) {
            if (i == 0) {
                max = tabela[0];
            } else if (tabela[i] > max) {
                max = tabela[i];
            }
        }
        return max;
    }

}
