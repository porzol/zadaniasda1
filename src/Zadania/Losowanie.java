package Zadania;

import java.util.Random;
import java.util.Scanner;

public class Losowanie {


    public static void main(String[] args) {
        Random liczba = new Random();
        int losowa = liczba.nextInt(100);
        System.out.println(losowa);
        int strzal = 0;
        while (strzal != losowa) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Podaj swój strzal z zakresu 0 do 99");
            strzal = sc.nextInt();
            if (strzal > losowa) {
                System.out.println("Podałeś za dużą wartość");
            } else if (strzal < losowa) {
                System.out.println("Podałeś za mała wartość");
            } else {
                System.out.println("Gratulacje trafiłeś");
            }
        }
    }
}
