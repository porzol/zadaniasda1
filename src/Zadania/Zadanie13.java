package Zadania;

import java.util.Scanner;

// Napisać program pobierający od użytkownika dwie liczby całkowite A oraz B, A < B, a następnie wyznaczający sumę ciągu liczb od A do B, czyli sumę ciągu (A,A + 1,...,B). Obliczenia należy wykonać dwukrotnie stosując kolejno pętle: while, for.
//        Przykład: Dla A = 4 i B = 11 program powinien wyświetlić: 60 60
public class Zadanie13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj 1 liczbe całkowita: ");
        int a = sc.nextInt();
        System.out.println("Podaj 2 liczbe całkowita większą niż " + a);
        int b = sc.nextInt();
        System.out.print(sumaLiczbFor(a,b)+ " ");
        System.out.print(sumaLiczbWhile(a,b));
    }
   public static int sumaLiczbFor (int a, int b){
       int suma = 0;
        for(int i = a; i <= b; i++){
            suma += i;
        }
        return suma;
   }
    public static int sumaLiczbWhile (int a, int b){
        int suma = 0;
        int i = a;
        while (i != b+1){
            suma += i;
            i++;
        }
        return suma;
    }
}
