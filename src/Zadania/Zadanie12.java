package Zadania;

import java.util.Scanner;

public class Zadanie12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
//        System.out.println("Podaj całkowitą liczbę dodatnia: ");
//        int x = sc.nextInt();
//        nieparzysteMniejsze(x);
//        int b = 100;
//        int[] tab1 = new int[b + 1];
//        System.out.println("Liczby podzielne przez 3 i 5 z zakresu 3 do 100 to: ");
//        for (int i = 2; i <= b; i++) {
//            tab1[i] = i;
//            if (i % 3 == 0 || i % 5 == 0) {
//                System.out.println(i);
//            }
//        }

        //12C
        System.out.println(" podaj pierwsza liczbe zakresu: ");
        int poczatek = sc.nextInt();
        System.out.println(" podaj druga liczbe zakresu: ");
        int koniec = sc.nextInt();
        System.out.println("Liczby podzielne przez 6 z zakresu od " + poczatek + " do " + koniec + " to:");
        for (int i = poczatek; i <=koniec; i++){
            if(i % 6 == 0){
                System.out.println(i);
            }
        }
    }

    public static void nieparzysteMniejsze(int a) {
        int[] tablica = new int[a + 1];
        for (int i = 0; i < a + 1; i++) {
            tablica[i] = i;
            if (i % 2 == 1) {
                System.out.println(i);
            }
        }
    }

}
