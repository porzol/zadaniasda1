package Zadania;

import java.util.Scanner;

public class Zadanie29 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj słowo do sprawzdzenia");
        String slowo = sc.nextLine().toLowerCase();
        System.out.println(slowo);
        char[] tab = slowo.toCharArray();
        int liczbaPowtorzen = 0;
        char ost = tab[tab.length-1];
        for(int i = 0; i < tab.length; i++){
            if (tab[i] == ost)
            liczbaPowtorzen++;
        }
        System.out.println("Ostatnia litera tego slowa " + ost + " wystepuje " + liczbaPowtorzen);

    }
}
