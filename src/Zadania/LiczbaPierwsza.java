package Zadania;

import java.util.Scanner;

public class LiczbaPierwsza {
    public static void main(String[] args)
    {
        Scanner dane = new Scanner(System.in);
        int x=dane.nextInt();

        boolean pierwsza = true;
        for(int i=2;i*i<=x;i++)
            if(x%i==0)
                pierwsza = false;

        if(pierwsza)
            System.out.println("TAK");
        else
            System.out.println("NIE");
    }
}
