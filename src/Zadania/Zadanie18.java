package Zadania;

import java.util.Scanner;

public class Zadanie18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj ilość liczb");
        int[] tab = new int[sc.nextInt()];
        System.out.println("podaj kolejne liczby z tabeli");
        for (int i = 0; i < tab.length; i++) {
            System.out.println("podaj " + (i + 1) + " liczbe");
            tab[i] = sc.nextInt();
        }
        System.out.println(min(tab));
        System.out.println(max(tab));
        System.out.println(srednia(max(tab), min(tab)));
    }

    public static int min(int[] tabela) {
        int min = 0;
        for (int i = 0; i < tabela.length; i++) {
            if (i == 0) {
                min = tabela[0];
            } else if (tabela[i] <= min) {
                min = tabela[i];
            }
        }
        return min;
    }

    public static int max(int[] tabela) {
        int max = 0;
        for (int i = 0; i < tabela.length; i++) {
            if (i == 0) {
                max = tabela[0];
            } else if (tabela[i] > max) {
                max = tabela[i];
            }
        }
        return max;
    }

    public static double srednia(int max, int min) {
        double arytmetic = (max + min) / 2;
        return arytmetic;
    }
}
