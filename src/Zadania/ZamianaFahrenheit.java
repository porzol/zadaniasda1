package Zadania;

import java.util.Scanner;

public class ZamianaFahrenheit {
    public static void main(String[] args) {
        Scanner podaj = new Scanner(System.in);
        System.out.println("Podaj wartość temperatury do konwersji: ");
        double temp = podaj.nextDouble();
        double tempF = konwersja(temp);
        System.out.println("temperatura skowertowana to : " + tempF  + " stopni F" );
    }
    public static double konwersja (double temp){
        double tempF = 1.8 * temp + 32;
        return tempF;
    }
}
