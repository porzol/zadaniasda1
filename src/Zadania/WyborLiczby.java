package Zadania;

import java.util.Scanner;

public class WyborLiczby {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("podaja 1 liczbe:");
        int a = scanner.nextInt();
        System.out.println("podaja 2 liczbe:");
        int b = scanner.nextInt();
        System.out.println("podaja 3 liczbe:");
        int c = scanner.nextInt();
        System.out.println(a + " "+ b + " " + c + " ");
        System.out.println("Największa liczba z podanych to: " + najwiekszaLiczba(a,b,c));
    }
    public static int najwiekszaLiczba(int a, int b, int c){
        int[] tab = new int[]{a,b,c};
        int d = 0;
        for (int i:tab) {
            if (i > d){
                d = i;
            }
        }
        return d;
    }
}
