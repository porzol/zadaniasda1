package Zadania;

import java.util.Scanner;

public class Zadanie22 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int liczba = sc.nextInt();
        for (int dzielnik = 1; liczba >= dzielnik; dzielnik++) {
            if (liczba % dzielnik == 0) {
                System.out.println(dzielnik);
            }
        }
    }
}
