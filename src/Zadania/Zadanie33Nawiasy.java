package Zadania;

import java.util.Scanner;

public class Zadanie33Nawiasy {
    public static void main(String[] args) {


        Scanner sc = new Scanner(System.in);
        System.out.println("Wypisz działanie z nawiasami");
        String dzialanie = sc.nextLine();

        int otwarty = 0;
        int zamkniety = 0;
        int roznica = 0;

        for (int i = 0; i < dzialanie.length(); i++){
            char znak = dzialanie.charAt(i);
            if (znak == '('){
                otwarty++;
                roznica++;
            }
            if(znak == ')'){
                zamkniety++;
                roznica--;
            }
            if (roznica < 0){
                break;
            }
        }
        if (otwarty == zamkniety && roznica == 0){
            System.out.println("działanie jest poprawne");
        }else {
            System.out.println("Dzialanie niepoprawne");
        }


    }
}
